package ru.vstu;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.poi.ss.usermodel.*;
import org.jooby.Jooby;
import org.jooby.Upload;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author jooby generator
 */
public class App extends Jooby {
  private String POAS_CODE = "26";
  {
    Logger.getLogger(PhantomJSDriverService.class.getName()).setLevel(Level.ALL);
    String converterFileName = ".doctopdf.exe";

    File configFile = new File("config.json");
    Scanner scanner = null;
    try {
      scanner = new Scanner(configFile);
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    StringBuilder fileContents = new StringBuilder((int) configFile.length());
    String lineSeparator = System.getProperty("line.separator");
    while (scanner.hasNextLine()) {
        fileContents.append(scanner.nextLine() + lineSeparator);
    }
    String configString = fileContents.toString();
    JsonObject config = new JsonParser().parse(configString).getAsJsonObject();
    scanner.close();

    DocDumpUploader.initialize(config);

    get("/", () -> "<form enctype=\"multipart/form-data\" action=\"/upload\" method=\"post\">\n" +
            "  <input name=\"file\" type=\"file\"/>\n<input type=\"submit\" value=\"Submit\">" +
            "</form>");
    post("/", (req, res) -> {
      JsonObject desc = new JsonParser().parse(req.param("desc").value()).getAsJsonObject();
      //System.out.println(desc.toString());
      Upload document = req.file(desc.get("document").getAsString());
      ArrayList<Upload> uploads = new ArrayList<>();
      uploads.add(document);

      HashMap<File, List<Integer>> titleMap = new HashMap<>();
      JsonObject titleMapJson = desc.get("titles").getAsJsonObject();
      for(String filename: titleMapJson.keySet()){
        ArrayList<Integer> l = new ArrayList<>();
        for(JsonElement pagenum: titleMapJson.get(filename).getAsJsonArray()){
          l.add(pagenum.getAsInt());
        }
        Upload titleFile = req.file(filename);
        titleMap.put(titleFile.file(), l);
        uploads.add(titleFile);
      }
      
      File result = DocConverter.doConvert(document.file(), titleMap, converterFileName);
      //File renamedResult = new File(desc.get("document").getAsString().replaceAll("\\..*$", ".pdf"));
      //result.renameTo(renamedResult);
      res.download(result);
      for(Upload up: uploads){
        up.close();
      }
      //res.send("kek");
    });

    post("/dumpUpload", (req, res) -> {
      DocDumpUploader docDumpUploader = new DocDumpUploader();
      Upload upload = req.file("document");
      //System.out.println(req.param("path").value());
      //System.out.println(upload.file().getName());

      File document = new File(upload.name());
      Files.copy(upload.file().toPath(), document.toPath());
      docDumpUploader.doUpload(document, req.param("path").value());
      upload.close();
      document.delete();
      res.send(true);
    });

    post("/parsePlan", (req, res) -> {
      Upload upload = req.file("document");
      //System.out.println(upload.file().getName());

      JsonArray classes = new JsonArray();
      File document = upload.file();
      Workbook wb = WorkbookFactory.create(new FileInputStream(document));
      Sheet plan = wb.getSheet("План");
      Cell classNameCell = findCell(plan, "Наименование");
      Cell codeCell = findCell(plan, "Код");
      if (codeCell != null && classNameCell != null){
        for(int i=plan.getFirstRowNum(); i<=plan.getLastRowNum(); i++){
          Cell c = plan.getRow(i).getCell(codeCell.getColumnIndex());
          if(c.getCellTypeEnum() == CellType.STRING && (c.getRichStringCellValue().getString().trim().equals(POAS_CODE) || c.getRichStringCellValue().getString().trim().isEmpty())){
            classes.add(plan.getRow(i).getCell(classNameCell.getColumnIndex()).getRichStringCellValue().getString().trim());
          }
        }
      }

      JsonObject response = new JsonObject();
      response.add("classes", classes);
      document.delete();
      //System.out.println(response.toString());
      res.send(response.toString());
    });

    post("dumpDelete", (req, res) -> {
      //System.out.println(req.param("path").value()+" "+req.param("filename").value());
      DocDumpUploader docDumpUploader = new DocDumpUploader();
      docDumpUploader.doDelete(req.param("path").value(), req.param("filename").value());
      res.send(true);
    });

  }


  private Cell findCell(Sheet sheet, String cellContent) {
    for (Row row : sheet) {
      for (Cell cell : row) {
        if (cell.getCellTypeEnum() == CellType.STRING) {
          if (cell.getRichStringCellValue().getString().trim().equals(cellContent)) {
            return cell;
          }
        }
      }
    }
    return null;
  }

  public static void main(final String[] args) {
    if(System.getProperty("os.name").toLowerCase().contains("windows")) {
      InputStream converterStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("doctopdf.exe");
      InputStream phantomJSStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("phantomjs.exe");
      File converterFile = new File(".doctopdf.exe");
      File phantomJSFile = new File("phantomjs.exe");
      byte[] buffer;
      try {
        buffer = new byte[converterStream.available()];

        converterStream.read(buffer);

        OutputStream fOutStream = new FileOutputStream(converterFile);
        fOutStream.write(buffer);

        fOutStream.close();
        converterStream.close();

        buffer = new byte[phantomJSStream.available()];

        phantomJSStream.read(buffer);

        fOutStream = new FileOutputStream(phantomJSFile);
        fOutStream.write(buffer);

        fOutStream.close();
        phantomJSStream.close();


      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    run(App::new, args);
  }

}
