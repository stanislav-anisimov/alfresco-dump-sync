package ru.vstu;

//import org.aioobe.cloudconvert.CloudConvertService;
//import org.aioobe.cloudconvert.ConvertProcess;
//import org.aioobe.cloudconvert.ProcessStatus;

import fr.opensagres.poi.xwpf.converter.pdf.PdfConverter;
import fr.opensagres.poi.xwpf.converter.pdf.PdfOptions;
import fr.opensagres.xdocreport.converter.IConverter;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.multipdf.Splitter;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class DocConverter
{
    public static File doConvert(File input, HashMap<File, List<Integer> > titles, String converterFilename) throws IOException {
        //input doc, output pdf, {title pdf, position in doc to replace title}
        File converted = new File(input.getAbsolutePath()+".converted");
        DocConverter.convert(input, converted, converterFilename);
        File result = new File(input.getAbsolutePath().replaceAll("\\..*$", ".pdf"));
        PDDocument resultPDF = insertTitlePages(converted, titles);
        resultPDF.save(result);
        resultPDF.close();
        converted.delete();
        return result;
    }

    private static PDDocument insertTitlePages(File documentFile, HashMap<File, List<Integer> > titles) throws IOException {
        PDDocument document = PDDocument.load(documentFile);

        List<PDDocument> pages = new Splitter().split(document);
        for(File key: titles.keySet()) {
            for(Integer pageNum: titles.get(key)){
                String extension = key.getName().substring(key.getName().length()-4);
                if(extension.equals(".png") || extension.equals(".jpg")){
                    PDPage page = new PDPage();
                    PDDocument doc = pages.get(pageNum);
                    doc.removePage(0);
                    doc.addPage(page);
                    PDImageXObject pdImage = PDImageXObject.createFromFile(key.getAbsolutePath(), doc);
                    PDPageContentStream contentStream = new PDPageContentStream(doc, page, PDPageContentStream.AppendMode.APPEND, true, true);
                    contentStream.drawImage(pdImage, 0, 0, 550, 800);
                    contentStream.close();
                } else {
                    PDDocument doc = pages.get(pageNum);
                    doc.close();
                    doc = PDDocument.load(key);
                    pages.set(pageNum, doc);
                    for(int i=1; i< doc.getNumberOfPages(); i++){
                        pages.remove(pageNum+1);
                    }
                }
            }
        }

        PDFMergerUtility merger = new PDFMergerUtility();
        merger.setDestinationFileName(documentFile.getName());
        PDDocument result = new PDDocument();
        for(PDDocument doc: pages){
            for(int i=0; i<doc.getNumberOfPages(); i++){
                result.addPage(doc.getPage(i));
            }
        }

        return result;
    }

    private static void convert(File inputFile, File outputFile, String converterFilename){
        try
        {
            if(System.getProperty("os.name").toLowerCase().contains("windows")) {
                ProcessBuilder converterBuilder = new ProcessBuilder(converterFilename, inputFile.getAbsolutePath(), outputFile.getAbsolutePath()).inheritIO();

                Process converter = converterBuilder.start();

                converter.waitFor();
            } else {
                InputStream doc = new FileInputStream(inputFile);
                XWPFDocument document = new XWPFDocument(doc);
                PdfOptions options = PdfOptions.create();
                OutputStream out = new FileOutputStream(outputFile);
                PdfConverter.getInstance().convert(document, out, options);
            }

        } catch (Exception e)
        {
            e.printStackTrace();
        }

        //CLOUD
//
//        CloudConvertService service = new CloudConvertService("Uv0JDZcK9EG8f0PNsBpzY5BxJeodU7fZvI0dhaFgNkUPVF7Pvcrdwv0y_LN6P94b3J5kZOmze8U3oYIBXmrdiA");
//        ConvertProcess process = null;
//        try {
//            process = service.startProcess("doc", "pdf");
//            process.startConversion(inputFile);
//            ProcessStatus status;
//            waitLoop: while (true) {
//                status = process.getStatus();
//                switch (status.step) {
//                    case FINISHED: break waitLoop;
//                    case ERROR: throw new RuntimeException(status.message);
//                }
//                Thread.sleep(200);
//            }
//            service.download(status.output.url, outputFile);
//
//            process.delete();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }
}