package ru.vstu;

import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;

public class DocDumpSync {
    public FolderNode parse(File folder, String pathToRoot){
        File[] childFolders = folder.listFiles(File::isDirectory);
        FolderNode result = new FolderNode(folder, pathToRoot);
        if(childFolders != null)
            for(File childFolder: childFolders){
                result.childrenDir.add(parse(childFolder, result.pathToNode));
            }
        File[] childFiles = folder.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.isFile() && pathname.getName().substring(pathname.getName().length()-4).equals(".pdf");
            }
        });
        if(childFiles != null){
            result.childrenFile.addAll(Arrays.asList(childFiles));
        }
        return result;
    }
}