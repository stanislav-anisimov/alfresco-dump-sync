package ru.vstu;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DocDumpUploader implements DocUploader {
    private static String dumpURL;
    private static String loginDump;
    private static String passwordDump;
    private WebDriver webdriver;

    public static void initialize(JsonObject config){
        dumpURL = config.get("dumpURL").getAsString();
        loginDump = config.get("loginDump").getAsString();
        passwordDump = config.get("passwordDump").getAsString();
    }

    public DocDumpUploader() {
    }

    @Override
    public void doUpload(File input, String path) {
        this.webdriver = new PhantomJSDriver();
        this.login();
        this.upload(input, path);
        this.webdriver.quit();
    }

    @Override
    public void doDelete(String path, String fileName) {
        this.webdriver = new PhantomJSDriver();
        this.login();
        this.remove(path, fileName);
        this.webdriver.quit();
    }

    private void remove(String path, String fileName){
        this.webdriver.get(dumpURL);
        this.cd(Arrays.asList(path.split("/")));
        List<WebElement> fileLink = this.webdriver.findElements(By.cssSelector("a[href*='"+transliterate(fileName)+"']"));
        if(!fileLink.isEmpty()){
            System.out.println(fileLink.get(0).getAttribute("href"));
            String deleteUrl = fileLink.get(0).getAttribute("href").replaceFirst("storage", "storage-delete");
            this.webdriver.get(deleteUrl);
            this.webdriver.findElement(By.id("edit-submit")).click();
        }
    }

    public void destroy(){
        this.webdriver.quit();
    }

    private void upload(File file, String path) {
        this.webdriver.get(dumpURL);

        this.cd(Arrays.asList(path.split("/")));

        List<WebElement> fileLink = this.webdriver.findElements(By.cssSelector("a[href*='"+transliterate(file.getName())+"']"));
        if(!fileLink.isEmpty()){
            String deleteUrl = fileLink.get(0).getAttribute("href").replaceFirst("storage", "storage-delete");
            this.webdriver.get(deleteUrl);
            this.webdriver.findElement(By.id("edit-submit")).click();
        }

        this.webdriver.findElement(By.id("edit-name")).sendKeys(file.getName());
        WebElement selectFile = this.webdriver.findElement(By.id("edit-upload"));
        selectFile.sendKeys(file.getAbsolutePath());
        selectFile.submit();
    }

    private void cd(List<String> path){
        for(String nextDir: path){
            if(nextDir.isEmpty())
                continue;

            List<WebElement> dirLink = this.webdriver.findElements(By.cssSelector("a[href*='"+transliterate(nextDir)+"']"));
            if(dirLink.isEmpty()){
                WebElement createFolderName = this.webdriver.findElement(By.id("edit-name-1"));
                createFolderName.sendKeys(nextDir);
                createFolderName.submit();
                this.webdriver.findElement(By.cssSelector("a[href*='"+transliterate(nextDir)+"']")).click();
            } else {
                dirLink.get(0).click();
            }
        }
    }

    private void login() {
        this.webdriver.get(dumpURL);
        if(!webdriver.findElements(By.id("block-user-0")).isEmpty()){
            WebElement loginField = webdriver.findElement(By.id("edit-name"));
            loginField.sendKeys(loginDump);

            WebElement passwordField = webdriver.findElement(By.id("edit-pass"));
            passwordField.sendKeys(passwordDump);

            passwordField.submit();
        }
    }

    public static String transliterate(String message){
        char[] abcCyr =   {' ', 'ё', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '=', 'й', 'ц', 'у', 'к', 'е', 'н', 'г', 'ш', 'щ', 'з', 'х', 'ъ', 'ф', 'ы', 'в', 'а', 'п', 'р', 'о', 'л', 'д', 'ж', 'э', '\\', 'я', 'ч', 'с', 'м', 'и', 'т', 'ь', 'б', 'ю', '.', 'Ё', '№', 'Й', 'Ц', 'У', 'К', 'Е', 'Н', 'Г', 'Ш', 'Щ', 'З', 'Х', 'Ъ', 'Ф', 'Ы', 'В', 'А', 'П', 'Р', 'О', 'Л', 'Д', 'Ж', 'Э', '/', 'Я', 'Ч', 'С', 'М', 'И', 'Т', 'Ь', 'Б', 'Ю', ',', '`', '~', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '+', 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '[', ']', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';', '\'', 'z', 'x', 'c', 'v', 'b', 'n', 'm', 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', '{', '}', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', ':', '"', '|', 'Z', 'X', 'C', 'V', 'B', 'N', 'M', '<', '>', '?'};
        String[] abcLat = {"_", "yo", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "-", "", "y", "c", "u", "k", "e", "n", "g", "sh", "shch", "z", "h", "", "f", "y", "v", "a", "p", "r", "o", "l", "d", "zh", "e", "", "ya", "ch", "s", "m", "i", "t", "", "b", "yu", ".", "Yo", "no", "Y", "C", "U", "K", "E", "N", "G", "Sh", "Shch", "Z", "H", "", "F", "Y", "V", "A", "P", "R", "O", "L", "D", "Zh", "E", "", "Ya", "Ch", "S", "M", "I", "T", "", "B", "Yu", "", "", "", "", "", "", "", "", "", "", "", "", "", "_", "", "q", "w", "e", "r", "t", "y", "u", "i", "o", "p", "", "", "a", "s", "d", "f", "g", "h", "j", "k", "l", "", "", "z", "x", "c", "v", "b", "n", "m", "Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P", "", "", "A", "S", "D", "F", "G", "H", "J", "K", "L", "", "", "", "Z", "X", "C", "V", "B", "N", "M", "", "", ""};
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < message.length(); i++) {
            for (int x = 0; x < abcCyr.length; x++ ) {
                if (message.charAt(i) == abcCyr[x]) {
                    builder.append(abcLat[x]);
                }
            }
        }
        return builder.toString();
    }


}
