package ru.vstu;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class DocMoodleUploader implements DocUploader{
    private String moodleURL;
    private String loginMoodle;
    private String passwordMoodle;
    private WebDriver webdriver;
    //private static String cookiesMoodle;

    public DocMoodleUploader(JsonObject config) throws FileNotFoundException {
        this.webdriver = new PhantomJSDriver();

        this.moodleURL = config.get("moodleURL").getAsString();
        this.loginMoodle = config.get("loginMoodle").getAsString();
        this.passwordMoodle = config.get("passwordMoodle").getAsString();
    }

    public void destroy(){
        this.webdriver.quit();
    }

    public void doUpload(File file, String path) {
        this.login();
        this.upload(file);
    }

    @Override
    public void doDelete(String path, String fileName) {

    }

    private void login() {
        try {
            this.webdriver.get(moodleURL+"/login/index.php");

            WebElement loginField = this.webdriver.findElement(By.id("username"));
            loginField.sendKeys(this.loginMoodle);

            WebElement passwordField = this.webdriver.findElement(By.id("password"));
            passwordField.sendKeys(this.passwordMoodle);

            passwordField.submit();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void upload(File file) {
        try {
            //access my files
            this.webdriver.get(moodleURL + "/user/files.php");


            //press add file
            WebElement addFileBtn = (new WebDriverWait(webdriver, 10))
                    .until(ExpectedConditions.visibilityOfElementLocated(By.className("fp-btn-add"))).findElement(By.tagName("a"));//this.webdriver.findElement(By.className("fp-btn-add"));
            addFileBtn.click();

            //press select file
            WebElement selectFile = (new WebDriverWait(webdriver, 10))
                    .until(ExpectedConditions.presenceOfElementLocated(By.name("repo_upload_file")));//this.webdriver.findElement(By.name("repo_upload_file"));
            selectFile.sendKeys(file.getAbsolutePath());

            //submit
            WebElement uploadButton = (new WebDriverWait(webdriver, 10))
                    .until(ExpectedConditions.presenceOfElementLocated(By.className("fp-upload-btn")));//this.webdriver.findElement((By.className("fp-upload-btn")));
            uploadButton.click();

            //save
            WebElement saveButton = (new WebDriverWait(webdriver, 10))
                    .until(ExpectedConditions.presenceOfElementLocated(By.name("submitbutton")));
            ((JavascriptExecutor)webdriver).executeScript("arguments[0].click()", saveButton);

            this.webdriver.quit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}