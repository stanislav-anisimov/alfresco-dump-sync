package ru.vstu;

import java.io.File;

public interface DocUploader {
    public void doUpload(File input, String path);
    public void doDelete(String path, String fileName);
}
