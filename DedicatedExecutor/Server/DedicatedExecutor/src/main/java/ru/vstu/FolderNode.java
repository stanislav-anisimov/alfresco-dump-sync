package ru.vstu;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Thread.sleep;

public class FolderNode {
    FolderNode(File file, String pathToRoot){
        this.file = file;
        this.childrenDir = new ArrayList<>();
        this.childrenFile = new ArrayList<>();
        if(file.getName().equals("root"))
            this.pathToNode = pathToRoot;
        else
            this.pathToNode = pathToRoot + "/" + transliterate(this.file.getName());
    }

    public File file;
    public List<FolderNode> childrenDir;
    public List<File> childrenFile;
    String pathToNode;

    public static String transliterate(String message){
        char[] abcCyr =   {'_','.','1','2','3','4','5','6','7','8','9','0','-',' ','а','б','в','г','д','е','ё', 'ж','з','и','й','к','л','м','н','о','п','р','с','т','у','ф','х', 'ц','ч', 'ш','щ','ъ','ы','ь','э', 'ю','я','А','Б','В','Г','Д','Е','Ё', 'Ж','З','И','Й','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Х', 'Ц', 'Ч','Ш', 'Щ','Ъ','Ы','Ь','Э','Ю','Я','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
        String[] abcLat = {"_",".","1","2","3","4","5","6","7","8","9","0","-","_","a","b","v","g","d","e","yo","zh","z","i","y","k","l","m","n","o","p","r","s","t","u","f","h","c","ch","sh","shch", "","y", "","e","yu","ya","A","B","V","G","D","E","Yo","Zh","Z","I","Y","K","L","M","N","O","P","R","S","T","U","F","H","C","Ch","Sh","Shch", "","Y", "","E","Yu","Ya","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < message.length(); i++) {
            for (int x = 0; x < abcCyr.length; x++ ) {
                if (message.charAt(i) == abcCyr[x]) {
                    builder.append(abcLat[x]);
                }
            }
        }
        return builder.toString();
    }

    public void sync(WebDriver webdriver){

        for(FolderNode folderNode: this.childrenDir){
            webdriver.get(this.pathToNode);
            System.out.println(folderNode.pathToNode);
            WebElement createFolderName = webdriver.findElement(By.id("edit-name-1"));
            createFolderName.sendKeys(folderNode.file.getName());
            createFolderName.submit();
            folderNode.sync(webdriver);
        }

        webdriver.get(this.pathToNode);
        for(File childFile: this.childrenFile){
            System.out.println(childFile.getName());
            webdriver.findElement(By.id("edit-name")).sendKeys(childFile.getName());
            WebElement selectFile = webdriver.findElement(By.id("edit-upload"));
            selectFile.sendKeys(childFile.getAbsolutePath());
            selectFile.submit();
        }
    }
}

