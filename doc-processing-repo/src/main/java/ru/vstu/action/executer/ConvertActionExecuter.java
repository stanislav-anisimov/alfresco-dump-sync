package ru.vstu.action.executer;


import bayou.form.FormData;
import bayou.form.FormDataFile;
import bayou.http.HttpClient;
import bayou.http.HttpRequest;
import bayou.http.HttpResponse;
import bayou.mime.ContentType;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.action.executer.ActionExecuterAbstractBase;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.repo.version.VersionModel;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ActionServiceException;
import org.alfresco.service.cmr.action.ParameterDefinition;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.cmr.tagging.TaggingService;
import org.alfresco.service.cmr.version.Version;
import org.alfresco.service.cmr.version.VersionService;
import org.alfresco.service.cmr.version.VersionType;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;

import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConvertActionExecuter extends ActionExecuterAbstractBase {

    private NodeService nodeService;
    private FileFolderService fileFolderService;
    private ContentService contentService;
    private TaggingService taggingService;
    private VersionService versionService;

    private String DEDICATED_EXECUTOR_ADDRESS = "http://109.206.169.194:8095/";

    @Override
    protected void executeImpl(Action action, NodeRef nodeRef) {
        if(!taggingService.getTags(nodeRef).contains("compile")){
            throw new ActionServiceException("Файл не помечен тегом compile");
        }
        if(!((String)nodeService.getProperty(nodeRef, ContentModel.PROP_NAME)).contains(".doc")){
            throw new ActionServiceException("Неверный формат файла");
        }
        //get title insert page from tag
        NodeRef currentDir = nodeService.getPrimaryParent(nodeRef).getParentRef();

        HashMap<String,ArrayList<Integer> > titlesString = new HashMap<>();
        String docFileNameFull = (String)nodeService.getProperty(nodeRef, ContentModel.PROP_NAME);

        for (ChildAssociationRef child : nodeService.getChildAssocs(currentDir)) {
            NodeRef childRef = child.getChildRef();
            if(!fileFolderService.getFileInfo(childRef).isFolder()){
                List<String> tags = taggingService.getTags(childRef);
                String childFileName = (String)nodeService.getProperty(childRef, ContentModel.PROP_NAME);
                for (String tag: tags){
                    if(tag.contains("-")){
                        String tagFileName = tag.substring(0, tag.indexOf('-'));
                        if(tagFileName.equalsIgnoreCase(docFileNameFull)){
                            int pageToInsert = Integer.parseInt(tag.substring(tag.indexOf("-")+1))-1;
                            if(titlesString.containsKey(childFileName)){
                                titlesString.get(childFileName).add(pageToInsert);
                            } else {
                                ArrayList<Integer> list = new ArrayList<>();
                                list.add(pageToInsert);
                                titlesString.put(childFileName, list);
                            }
                        }
                    }
                }
            }
        }

        String docFileName = docFileNameFull.substring(0,docFileNameFull.indexOf('.'));
        ContentReader docReader = contentService.getReader(nodeRef, ContentModel.PROP_CONTENT);
        File docFile = new File(docFileName+".doc");
        docReader.getContent(docFile);

        HashMap<File, ArrayList<Integer> > titles = new HashMap<>();
        for(String key: titlesString.keySet()){
            NodeRef pdfTitleNode = fileFolderService.searchSimple(currentDir, key);
            ContentReader pdfTitleReader = contentService.getReader(pdfTitleNode, ContentModel.PROP_CONTENT);
            File pdfTitleFile = new File(key);
            pdfTitleReader.getContent(pdfTitleFile);
            titles.put(pdfTitleFile, titlesString.get(key));
        }

        FormData form = new FormData("POST", DEDICATED_EXECUTOR_ADDRESS).charset(Charset.forName("UTF-8"));
        JsonObject descObj = new JsonObject();
        descObj.addProperty("document", docFile.getName());
        JsonObject titlesObj = new JsonObject();
        for(File titleFile: titles.keySet()){
            JsonArray pages = new JsonArray();
            form = form.file(titleFile.getName(), new FormDataFile(titleFile.getName(),ContentType.application_octet_stream,titleFile.toPath(),titleFile.length()));
            for(Integer pageNum: titles.get(titleFile)){
                pages.add(pageNum);
            }
            titlesObj.add(titleFile.getName(), pages);
        }
        descObj.add("titles", titlesObj);

        HttpClient client = new HttpClient();
        HttpRequest request = form
                .param("desc", descObj.toString())
                .file(docFile.getName(), new FormDataFile(docFile.getName(), ContentType.application_octet_stream, docFile.toPath(), docFile.length()))
                .toRequest();
        String pdfFileName = "";
        try {
            HttpResponse response = client.send(request).sync();
            //System.out.println(response.headers().toString());
            //System.out.println(response.statusCode());
            String resHeader = response.header("Content-Disposition");
            pdfFileName = resHeader.substring(resHeader.indexOf('"')+1, resHeader.substring(resHeader.indexOf('"')+1, resHeader.length()).indexOf('"')+resHeader.indexOf('"')+1);
            FileOutputStream fos = new FileOutputStream(new File(pdfFileName));
            fos.write(response.bodyBytes(Integer.MAX_VALUE).sync().array());
            fos.close();

        } catch (Exception e) {
            throw new ActionServiceException("Что-то пошло не так: см логи модуля генерации PDF");
        }


        File pdfResultFile = new File(pdfFileName);
        //write pdf

        NodeRef pdfResultNode = fileFolderService.searchSimple(currentDir, docFileName+".pdf");
        boolean pdfAlreadyExists = pdfResultNode != null;
        if (!pdfAlreadyExists) {
            pdfResultNode = fileFolderService.create(currentDir, docFileName+".pdf", QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, "content")).getNodeRef();
            nodeService.addAspect(pdfResultNode, ContentModel.ASPECT_VERSIONABLE,null);
            nodeService.addAspect(pdfResultNode, ContentModel.ASPECT_TAGGABLE,null);
        }

        ContentWriter writer = contentService.getWriter(pdfResultNode, ContentModel.PROP_CONTENT, true);
        writer.setMimetype(MimetypeMap.MIMETYPE_PDF);
        writer.putContent(pdfResultFile);

        Map<String, Serializable> versionProperties = new HashMap<>();
        versionProperties.put(Version.PROP_DESCRIPTION, "Файл автоматически обновлен");
        versionProperties.put(VersionModel.PROP_VERSION_TYPE, VersionType.MAJOR);
        if (pdfAlreadyExists) {
            versionService.createVersion(pdfResultNode, versionProperties);
        }

        docFile.delete();
        for(File title: titles.keySet()){
            title.delete();
        }
        pdfResultFile.delete();
        client.close();
    }

    @Override
    protected void addParameterDefinitions(List<ParameterDefinition> list) {

    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public void setFileFolderService(FileFolderService fileFolderService) {
        this.fileFolderService = fileFolderService;
    }

    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }

    public void setTaggingService(TaggingService taggingService) {
        this.taggingService = taggingService;
    }

    public void setVersionService(VersionService versionService) {this.versionService = versionService;}

}