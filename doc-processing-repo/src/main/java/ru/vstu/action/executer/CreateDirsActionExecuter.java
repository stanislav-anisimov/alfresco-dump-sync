package ru.vstu.action.executer;

import bayou.form.FormData;
import bayou.form.FormDataFile;
import bayou.http.HttpClient;
import bayou.http.HttpRequest;
import bayou.http.HttpResponse;
import bayou.mime.ContentType;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.alfresco.model.ContentModel;
import org.alfresco.query.PagingRequest;
import org.alfresco.repo.action.executer.ActionExecuterAbstractBase;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ActionServiceException;
import org.alfresco.service.cmr.action.ParameterDefinition;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileNotFoundException;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.cmr.tagging.TaggingService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CreateDirsActionExecuter extends ActionExecuterAbstractBase {
    private NodeService nodeService;
    private FileFolderService fileFolderService;
    private ContentService contentService;
    private TaggingService taggingService;
    private int MAX_FILEFOLDER_NAME_LENGTH = 200;
    private List<String> foldersWithoutPDFs = new ArrayList<>();
    private NodeRef originalCallNode = null;
    private String DEDICATED_EXECUTOR_ADDRESS = "http://localhost:8095/";

    private StringBuilder sb = new StringBuilder();

    @Override
    protected void executeImpl(Action action, NodeRef nodeRef) {
        JsonArray classes = null;
        if(!((String)nodeService.getProperty(nodeRef, ContentModel.PROP_NAME)).contains(".xls")){
            throw new ActionServiceException("Неверный формат файла");
        }

        try {
            ContentReader planReader = contentService.getReader(nodeRef, ContentModel.PROP_CONTENT);
            File plan = new File((String) nodeService.getProperty(nodeRef, ContentModel.PROP_NAME));
            planReader.getContent(plan);
            HttpClient client = new HttpClient();
            HttpRequest request = new FormData("POST", DEDICATED_EXECUTOR_ADDRESS + "parsePlan")
                    .charset(Charset.forName("UTF-8"))
                    .file("document", new FormDataFile(plan.getName(), ContentType.application_octet_stream, plan.toPath(), plan.length())).toRequest();
            HttpResponse response = client.send(request).sync();
            String resStr = response.bodyString(Integer.MAX_VALUE).sync();
            //System.out.println(resStr);
            JsonObject classesObj = new JsonParser().parse(resStr).getAsJsonObject();
            classes = classesObj.get("classes").getAsJsonArray();

            for(JsonElement _class: classes){
                String _classStr = _class.getAsString();
                //System.out.println(_classStr);
                if(fileFolderService.searchSimple(nodeService.getPrimaryParent(nodeRef).getParentRef(), _classStr)==null){
                    fileFolderService.create(nodeService.getPrimaryParent(nodeRef).getParentRef(), _classStr, QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, "folder"));
                }
            }

        } catch (Exception e) {
            throw new ActionServiceException("Неверный формат файла: см логи модуля разбора XLS");
        }
    }

    @Override
    protected void addParameterDefinitions(List<ParameterDefinition> list) {

    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public void setFileFolderService(FileFolderService fileFolderService) {
        this.fileFolderService = fileFolderService;
    }

    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }

    public void setTaggingService(TaggingService taggingService) {
        this.taggingService = taggingService;
    }
}
