package ru.vstu.action.executer;

import org.alfresco.model.ContentModel;
import org.alfresco.query.PagingRequest;
import org.alfresco.repo.action.executer.ActionExecuterAbstractBase;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ParameterDefinition;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileNotFoundException;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.cmr.tagging.TaggingService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;

import java.util.ArrayList;
import java.util.List;

public class CreateReportActionExecuter extends ActionExecuterAbstractBase {
    private NodeService nodeService;
    private FileFolderService fileFolderService;
    private ContentService contentService;
    private TaggingService taggingService;
    private String syncTag = "dumpsync";
    private String dumpRootTag = "dumproot";
    private String DEDICATED_EXECUTOR_ADDRESS = "http://localhost:8095/";
    private int MAX_FILEFOLDER_NAME_LENGTH = 200;
    private List<String> foldersWithoutPDFs = new ArrayList<>();
    private NodeRef originalCallNode = null;

    private StringBuilder sb = new StringBuilder();

    @Override
    protected void executeImpl(Action action, NodeRef nodeRef) {
        originalCallNode = nodeRef;
        foldersWithoutPDFs.clear();
        if (!fileFolderService.getFileInfo(nodeRef).isFolder()) {
            return;
        }
        checkPDFs(nodeRef);
        checkMethod(nodeRef);
        NodeRef reportNode = fileFolderService.searchSimple(nodeService.getPrimaryParent(nodeRef).getParentRef(), "Отчет.txt");
        if (reportNode == null) {
            reportNode = fileFolderService.create(nodeService.getPrimaryParent(nodeRef).getParentRef(), "Отчет.txt", QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, "content")).getNodeRef();
        }
        ContentWriter writer = contentService.getWriter(reportNode, ContentModel.PROP_CONTENT, true);
        writer.setMimetype(MimetypeMap.MIMETYPE_TEXT_PLAIN);
        writer.putContent(sb.toString());
    }

    private void checkPDFs(NodeRef node) {
        try {
            //if a folder has a tag AND contains no other folders AND contains no pdfs
            if (fileFolderService.getFileInfo(node).isFolder() && taggingService.getTags(node).contains(syncTag) &&
                    fileFolderService.list(node, false, true, "*", null, null, new PagingRequest(100)).getPage().isEmpty() &&
                    fileFolderService.list(node, true, false, "*.pdf", null, null, new PagingRequest(100)).getPage().isEmpty()) {

                this.sb.append("PDF отсутствуют в " + String.join("/", fileFolderService.getNameOnlyPath(originalCallNode, node))).append("\n\n");
            }
            for (ChildAssociationRef child : nodeService.getChildAssocs(node)) {
                checkPDFs(child.getChildRef());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void checkMethod(NodeRef node) {
        if(((String)nodeService.getProperty(node, ContentModel.PROP_NAME)).contains("Метод")){
            return;
        }
        try {
            //if a folder has a tag AND contains no folder named "Метод*" AND is not the original folder
            if (fileFolderService.getFileInfo(node).isFolder() && taggingService.getTags(node).contains(syncTag) &&
                    fileFolderService.list(node, false, true, "Метод*", null, null, new PagingRequest(100)).getPage().isEmpty() &&
                    node != originalCallNode) {

                this.sb.append("Отсутствуют файлы \"Метод*\" в " + String.join("/", fileFolderService.getNameOnlyPath(originalCallNode, node))).append("\n\n");
            }

            for (ChildAssociationRef child : nodeService.getChildAssocs(node)) {
                checkMethod(child.getChildRef());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void addParameterDefinitions(List<ParameterDefinition> list) {

    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public void setFileFolderService(FileFolderService fileFolderService) {
        this.fileFolderService = fileFolderService;
    }

    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }

    public void setTaggingService(TaggingService taggingService) {
        this.taggingService = taggingService;
    }
}
