package ru.vstu.action.executer;

import bayou.form.FormData;
import bayou.http.HttpClient;
import bayou.http.HttpRequest;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.node.NodeServicePolicies;
import org.alfresco.repo.policy.Behaviour;
import org.alfresco.repo.policy.JavaBehaviour;
import org.alfresco.repo.policy.PolicyComponent;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.ContentService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.tagging.TaggingService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DeleteAsset implements NodeServicePolicies.BeforeDeleteNodePolicy  {

    private PolicyComponent policyComponent;
    private Behaviour beforeDeleteNode;

    private NodeService nodeService;
    private FileFolderService fileFolderService;
    private ContentService contentService;
    private TaggingService taggingService;

    private String syncTag = "dumpsync";
    private String dumpRootTag = "dumproot";
    private String DEDICATED_EXECUTOR_ADDRESS = "http://localhost:8095/";

    public void init() {
        this.beforeDeleteNode = new JavaBehaviour(this,"beforeDeleteNode", Behaviour.NotificationFrequency.EVERY_EVENT);
        this.policyComponent.bindClassBehaviour(QName.createQName(NamespaceService.ALFRESCO_URI,"beforeDeleteNode"),
                QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, "content"), this.beforeDeleteNode);
        this.policyComponent.bindClassBehaviour(QName.createQName(NamespaceService.ALFRESCO_URI,"beforeDeleteNode"),
                QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, "folder"), this.beforeDeleteNode);
    }

    @Override
    public void beforeDeleteNode(NodeRef nodeRef) {
        System.out.println("beforeDeleteNode!");
        if(!taggingService.getTags(nodeRef).contains(syncTag)){
            return;
        }
        List<String> pathToParent = new ArrayList<>();
        NodeRef parent = nodeService.getPrimaryParent(nodeRef).getParentRef();
        while (parent != null && !taggingService.getTags(parent).contains(dumpRootTag)) {
            if (taggingService.getTags(parent).contains(syncTag)) {
                pathToParent.add((String) nodeService.getProperty(parent, ContentModel.PROP_NAME));
            }
            parent = nodeService.getPrimaryParent(parent).getParentRef();
        }
        Collections.reverse(pathToParent);
        syncNode(pathToParent, nodeRef);

    }

    private void syncNode(List<String> pathToParent, NodeRef node) {
        List<String> pathToNode = new ArrayList<>(pathToParent);
        if (fileFolderService.getFileInfo(node).isFolder()) {
            if (taggingService.getTags(node).contains(syncTag)) {
                pathToNode.add((String) nodeService.getProperty(node, ContentModel.PROP_NAME));
            }
            for (ChildAssociationRef child : nodeService.getChildAssocs(node)) {
                syncNode(pathToNode, child.getChildRef());
            }
        }
        if (taggingService.getTags(node).contains(syncTag)) {
            HttpClient client = new HttpClient();
            HttpRequest request = new FormData("POST", DEDICATED_EXECUTOR_ADDRESS + "dumpDelete")
                    .charset(Charset.forName("UTF-8"))
                    .param("path", String.join("/", pathToParent))
                    .param("filename", nodeService.getProperty(node, ContentModel.PROP_NAME)).toRequest();
            try {
                client.send(request).sync();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public void setFileFolderService(FileFolderService fileFolderService) {
        this.fileFolderService = fileFolderService;
    }

    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }

    public void setTaggingService(TaggingService taggingService) {
        this.taggingService = taggingService;
    }

    public void setPolicyComponent(PolicyComponent policyComponent) {
        this.policyComponent = policyComponent;
    }
}

