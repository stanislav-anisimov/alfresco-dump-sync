package ru.vstu.action.executer;

import bayou.form.FormData;
import bayou.form.FormDataFile;
import bayou.http.HttpClient;
import bayou.http.HttpRequest;
import bayou.mime.ContentType;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.action.executer.ActionExecuterAbstractBase;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ActionServiceException;
import org.alfresco.service.cmr.action.ParameterDefinition;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileNotFoundException;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.ContentService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.tagging.TaggingService;

import java.io.File;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class UploadActionExecuter extends ActionExecuterAbstractBase {
    private NodeService nodeService;
    private FileFolderService fileFolderService;
    private ContentService contentService;
    private TaggingService taggingService;
    private String syncTag = "dumpsync";
    private String dumpRootTag = "dumproot";
    private String DEDICATED_EXECUTOR_ADDRESS = "http://localhost:8095/";
    private int MAX_FILEFOLDER_NAME_LENGTH = 200;

    @Override
    protected void executeImpl(Action action, NodeRef nodeRef) {
        List<String> pathToParent = new ArrayList<>();
        if (!fileFolderService.getFileInfo(nodeRef).isFolder() && !taggingService.getTags(nodeRef).contains(syncTag)) {
            throw new ActionServiceException("Файл не помечен тегом синхронизации");
        }
        NodeRef parent = taggingService.getTags(nodeRef).contains(dumpRootTag) ? null : nodeService.getPrimaryParent(nodeRef).getParentRef();
        while (parent != null && !taggingService.getTags(parent).contains(dumpRootTag)) {
            if (((String) nodeService.getProperty(parent, ContentModel.PROP_NAME)).length() > MAX_FILEFOLDER_NAME_LENGTH) {
                throw new ActionServiceException("File/folder name " + nodeService.getProperty(parent, ContentModel.PROP_NAME) + " too long");
            }
            try{
                if (((String) nodeService.getProperty(parent, ContentModel.PROP_NAME)).contains("\uFEFF")) {
                    fileFolderService.rename(parent, ((String) nodeService.getProperty(parent, ContentModel.PROP_NAME)).substring(1));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (taggingService.getTags(parent).contains(syncTag)) {
                pathToParent.add((String) nodeService.getProperty(parent, ContentModel.PROP_NAME));
            }
            parent = nodeService.getPrimaryParent(parent).getParentRef();
        }
        Collections.reverse(pathToParent);
        syncNode(pathToParent, nodeRef);
    }

    private void syncNode(List<String> pathToParent, NodeRef node) {
        if (((String) nodeService.getProperty(node, ContentModel.PROP_NAME)).contains("\uFEFF")) {
            try {
                fileFolderService.rename(node, ((String) nodeService.getProperty(node, ContentModel.PROP_NAME)).substring(1));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        List<String> pathToNode = new ArrayList<>(pathToParent);
        if (fileFolderService.getFileInfo(node).isFolder()) {
            if (taggingService.getTags(node).contains(syncTag)) {
                pathToNode.add((String) nodeService.getProperty(node, ContentModel.PROP_NAME));
            }
            for (ChildAssociationRef child : nodeService.getChildAssocs(node)) {
                syncNode(pathToNode, child.getChildRef());
            }
        } else if (taggingService.getTags(node).contains(syncTag)) {
            try {
                File document = new File((String) nodeService.getProperty(node, ContentModel.PROP_NAME));
                contentService.getReader(node, ContentModel.PROP_CONTENT).getContent(document);
                HttpClient client = new HttpClient();
                HttpRequest request = new FormData("POST", DEDICATED_EXECUTOR_ADDRESS + "dumpUpload")
                        .charset(Charset.forName("UTF-8"))
                        .param("path", String.join("/", pathToNode))
                        .file("document", new FormDataFile(document.getName(), ContentType.application_octet_stream, document.toPath(), document.length())).toRequest();
                client.send(request).sync();
                document.delete();
                client.close();
            } catch (Exception e) {
                throw new ActionServiceException("Что-то пошло не так: см логи модуля загрузки файлов");
            }
        }
    }

    @Override
    protected void addParameterDefinitions(List<ParameterDefinition> list) {

    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public void setFileFolderService(FileFolderService fileFolderService) {
        this.fileFolderService = fileFolderService;
    }

    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }

    public void setTaggingService(TaggingService taggingService) {
        this.taggingService = taggingService;
    }
}
