﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Word;
using System.IO;

namespace doctopdf
{
    class Program
    {
        static void Main(string[] args)
        {
            if(args.Length == 0)
            {
                System.Console.WriteLine(Directory.GetCurrentDirectory());
                return;
            }

            Microsoft.Office.Interop.Word.Application app = new Microsoft.Office.Interop.Word.Application();
            Microsoft.Office.Interop.Word.Document wordDocument = app.Documents.Open(args[0]);
            wordDocument.ExportAsFixedFormat(args[1], WdExportFormat.wdExportFormatPDF);
            wordDocument.Close();
            app.Quit();
        }
    }
}
