package ru.vstu.PDFConverter;

import java.io.*;

public class Main {
    public static void main(String[] args) {
//        //input doc, output pdf, {title pdf, position in doc to replace title}
//        String inputDoc = new File(args[0]).getAbsolutePath();
//        String outputPDF = new File(args[1]).getAbsolutePath();
//        //PDFConverter.convert(inputDoc, outputPDF);
//        HashMap<String, ArrayList<Integer>> titles = new HashMap<>();
//        for(int i=2; i+1<args.length; i+=2){
//            String title = new File(args[i]).getAbsolutePath();
//            if(titles.containsKey(title)){
//                titles.get(title).add(Integer.parseInt(args[i+1])-1);
//            } else {
//                ArrayList<Integer> list = new ArrayList<>();
//                list.add(Integer.parseInt(args[i+1])-1);
//                titles.put(title, list);
//            }
//        }
//        try {
//            for(String key: titles.keySet()) {
//                for(Integer page: titles.get(key)){
//                    PDFConverter.insertTitlePage(new File(key), page, new File(outputPDF));
//                }
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        PDFUploader uploader = null;
        try {
            uploader = new PDFUploader(new File("config.json"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        uploader.uploadToMoodle(new File("pom.xml"));

    }


}
