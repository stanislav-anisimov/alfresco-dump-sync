package ru.vstu.PDFConverter;

import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;
import org.aioobe.cloudconvert.CloudConvertService;
import org.aioobe.cloudconvert.ConvertProcess;
import org.aioobe.cloudconvert.ProcessStatus;

import java.io.*;
import java.util.Iterator;
import java.util.List;

public class PDFConverter
{
    public static void insertTitlePage(File titleFile, int position, File outputFile) throws IOException {
        System.out.println(titleFile.getAbsolutePath());
        System.out.println(outputFile.getAbsolutePath());

        File temp=new File("temp.pdf");
        PdfDocument document = new PdfDocument(new PdfReader(outputFile), new PdfWriter(temp));
        titleFile = new File(titleFile.getAbsolutePath());
        PdfDocument title = new PdfDocument(new PdfReader(titleFile));
        for(int i=0;i<title.getNumberOfPages();i++){
            document.removePage(position);
        }
        title.copyPagesTo(1,title.getNumberOfPages(),document,position);
        document.close();
        title.close();
        temp.renameTo(outputFile);

//        PDDocument document = PDDocument.load(outputFile);
//        PDDocument title = PDDocument.load(titleFile);
//        Splitter splitter = new Splitter();
//        List<PDDocument> pages = splitter.split(document);
//
//        List<PDDocument> pagesBefore = pages.subList(0, position);
//        List<PDDocument> pagesAfter = pages.subList(position+title.getNumberOfPages(), pages.size());
//
//        PDFMergerUtility merger = new PDFMergerUtility();
//        PDDocument result = new PDDocument();
//
//        Iterator<PDDocument> iterator = pagesBefore.listIterator();
//        while(iterator.hasNext()) {
//            merger.appendDocument(result, iterator.next());
//        }
//
//
//        merger.appendDocument(result, title);
//
//        title.close();
//
//        iterator = pagesAfter.listIterator();
//        while(iterator.hasNext()) {
//            merger.appendDocument(result, iterator.next());
//        }
//
//
//        result.save(outputFile);
//        result.close();
//        document.close();
//        for(PDDocument doc: pages){
//            doc.close();
//        }
    }

    private static String loadFile(String filename){
        InputStream fInStream = Main.class.getResourceAsStream("/res/" + filename);
        File converterFile = new File("." + filename);
        byte[] buffer;
        try
        {
            buffer = new byte[fInStream.available()];

            fInStream.read(buffer);

            OutputStream fOutStream = new FileOutputStream(converterFile);
            fOutStream.write(buffer);

            fOutStream.close();
            fInStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return "."+filename;
    }
    public static void convert(File inputFile, File outputFile){
//        try
//        {
//            String converterFilename = loadFile("doctopdf.exe");
//            ProcessBuilder converterBuilder = new ProcessBuilder(converterFilename, inputFile, outputFile).inheritIO();
//
//            Process converter = converterBuilder.start();
//
//            converter.waitFor();
//
//            File converterFile = new File(converterFilename);
//
//            converterFile.delete();
//
//        } catch (IOException e)
//        {
//            e.printStackTrace();
//        } catch (InterruptedException e)
//        {
//
//        }

        CloudConvertService service = new CloudConvertService("Uv0JDZcK9EG8f0PNsBpzY5BxJeodU7fZvI0dhaFgNkUPVF7Pvcrdwv0y_LN6P94b3J5kZOmze8U3oYIBXmrdiA");
        ConvertProcess process = null;
        try {
            process = service.startProcess("doc", "pdf");
            process.startConversion(inputFile);
            ProcessStatus status;
            waitLoop: while (true) {
                status = process.getStatus();
                switch (status.step) {
                    case FINISHED: break waitLoop;
                    case ERROR: throw new RuntimeException(status.message);
                }
                Thread.sleep(200);
            }
            service.download(status.output.url, outputFile);

            process.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}