package ru.vstu.PDFConverter;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class PDFUploader {
    private String moodleURL;
    private String loginMoodle;
    private String passwordMoodle;
    private WebDriver webdriver;
    //private static String cookiesMoodle;

    public PDFUploader(File config) throws FileNotFoundException {
        this.webdriver = new PhantomJSDriver();

        Scanner scanner = new Scanner(config);
        StringBuilder fileContents = new StringBuilder((int) config.length());
        try {
            String lineSeparator = System.getProperty("line.separator");
            while (scanner.hasNextLine()) {
                fileContents.append(scanner.nextLine() + lineSeparator);
            }
        } finally {
            scanner.close();
        }
        String configString = fileContents.toString();
        JsonObject configJSON = new JsonParser().parse(configString).getAsJsonObject();
        this.moodleURL = configJSON.get("moodleURL").getAsString();
        this.loginMoodle = configJSON.get("loginMoodle").getAsString();
        this.passwordMoodle = configJSON.get("passwordMoodle").getAsString();
    }

    public void uploadToMoodle(File file) {
        this.loginMoodle();
        this.uploadMoodle(file);
    }

    private void loginMoodle() {
        try {
            this.webdriver.get(moodleURL+"/login/index.php");

            WebElement loginField = this.webdriver.findElement(By.id("username"));
            loginField.sendKeys(this.loginMoodle);

            WebElement passwordField = this.webdriver.findElement(By.id("password"));
            passwordField.sendKeys(this.passwordMoodle);

            passwordField.submit();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void uploadMoodle(File file) {
        try {
            //access my files
            this.webdriver.get(moodleURL + "/user/files.php");


            //press add file
            WebElement addFileBtn = (new WebDriverWait(webdriver, 10))
                    .until(ExpectedConditions.visibilityOfElementLocated(By.className("fp-btn-add"))).findElement(By.tagName("a"));//this.webdriver.findElement(By.className("fp-btn-add"));
            addFileBtn.click();

            //press select file
            WebElement selectFile = (new WebDriverWait(webdriver, 10))
                    .until(ExpectedConditions.presenceOfElementLocated(By.name("repo_upload_file")));//this.webdriver.findElement(By.name("repo_upload_file"));
            selectFile.sendKeys(file.getAbsolutePath());

            //submit
            WebElement uploadButton = (new WebDriverWait(webdriver, 10))
                    .until(ExpectedConditions.presenceOfElementLocated(By.className("fp-upload-btn")));//this.webdriver.findElement((By.className("fp-upload-btn")));
            uploadButton.click();

            //save
            WebElement saveButton = (new WebDriverWait(webdriver, 10))
                    .until(ExpectedConditions.presenceOfElementLocated(By.name("submitbutton")));
            ((JavascriptExecutor)webdriver).executeScript("arguments[0].click()", saveButton);

            this.webdriver.quit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}